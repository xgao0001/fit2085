def sum_items(a_list):
    sum = 0
    if len(a_list) > 0:
        for i in a_list:
            sum += i
    else:
        sum = 0

    return sum


def test_sum_items():
    print(sum_items([]))
    try:
        sum_items(["yeet"])
    except TypeError:
        print("pass")

import webbrowser

webbrowser.open('https://docs.google.com/document/d/1M0wxSlTC2x_2xep7VE2IbId2vOaz3D4hwX04Hcup29c/edit')

if __name__ == "__main__":
    test_sum_items()
